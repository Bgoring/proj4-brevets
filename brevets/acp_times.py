"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
from flask.globals import request
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
   """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
   """
   km = control_dist_km
   brevet_start_time = arrow.get(brevet_start_time)

   if km == 0:
      return brevet_start_time.isoformat()
   vals = {0:34, 1:32, 2:30, 3:28, 4:26}


   grow = halfit(km, vals)

   if km == 300:
      grow[1] -= 1

   brevet_start_time = brevet_start_time.shift(hours=+(grow[0]), minutes=+grow[1])
   return brevet_start_time.isoformat()



def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
   """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
   """
   km = control_dist_km
   brevet_start_time = arrow.get(brevet_start_time)
   if km == 0:
      brevet_start_time = brevet_start_time.shift(hours=+1)
      return brevet_start_time.isoformat()

   if km >= 200 and brevet_dist_km == 200:
       Close = 13,30
       brevet_start_time = brevet_start_time.shift(hours=+(Close[0]), minutes=+Close[1])
       return brevet_start_time.isoformat()

   if km >= 300 and brevet_dist_km == 300:
       Close = 20,0
       brevet_start_time = brevet_start_time.shift(hours=+(Close[0]), minutes=+Close[1])
       return brevet_start_time.isoformat()
   
   if km >= 400 and brevet_dist_km == 400:
      Close = 27,0
      brevet_start_time = brevet_start_time.shift(hours=+(Close[0]), minutes=+Close[1])
      return brevet_start_time.isoformat()

   if km >= 600 and brevet_dist_km == 600:
      Close = 40,0
      brevet_start_time = brevet_start_time.shift(hours=+(Close[0]), minutes=+Close[1])
      return brevet_start_time.isoformat()

   if km >= 1000 and brevet_dist_km == 1000:
      Close = 75,0
      brevet_start_time = brevet_start_time.shift(hours=+(Close[0]), minutes=+Close[1])
      return brevet_start_time.isoformat()

   vals = {0:15, 1:15, 2:15, 3:11.428, 4:13.333}
   grow = halfit(km, vals)

   
   brevet_start_time = brevet_start_time.shift(hours=+grow[0], minutes=+grow[1])
   return brevet_start_time.isoformat()


def timeFunc(km, time):
   
   retval = int(km/time),round(((km/time) - int(km/time)) * 60.0)

   return retval


def halfit(km, vals):
   tryy = []
   copy2 = km
   if km <= 200 and km > 0:
        length = 1
   elif km > 200 and km <= 400:
        length = 2
   elif km > 400 and km <= 600:
        length = 3
   elif km > 600 and km <= 1000:
        length = 4
   elif km > 1000 and km <= 1300:
       length = 5

   for i in range(length-1):
      copy2 -= 200
      tryy.append(200.0)
   tryy.append(copy2)

   grow = [0,0]
   for index in range(len(tryy)):
      x = timeFunc(tryy[index], vals[index])
      grow[0] += x[0]
      grow[1] += x[1]

   while grow[1] >= 60:
      grow[0] += 1
      grow[1] -= 60

   return grow