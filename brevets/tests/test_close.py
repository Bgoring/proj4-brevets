"""
Nose tests for letterbag.py
"""

from acp_times import close_time


import nose    # Testing framework
import logging
import arrow
logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.WARNING)
log = logging.getLogger(__name__)


def test_zero():
    """
    Close time == original time plus one hour
    """

    assert close_time(0, 200, arrow.get("2021-11-16T16:31:00-08:00")) == '2021-11-16T17:31:00-08:00'
    assert close_time(0, 300, arrow.get("2021-11-16T16:31:00-08:00")) == '2021-11-16T17:31:00-08:00'
    assert close_time(0, 400, arrow.get("2021-11-16T16:31:00-08:00")) == '2021-11-16T17:31:00-08:00'
    assert close_time(0, 600, arrow.get("2021-11-16T16:31:00-08:00")) == '2021-11-16T17:31:00-08:00'
    assert close_time(0, 1000, arrow.get("2021-11-16T16:31:00-08:00")) == '2021-11-16T17:31:00-08:00'


def test_defined_closes():
    """
    Use provided close time for 200, 300, 400, 600, and 1000km brevet rather then calculate
    """
    assert close_time(205, 200, arrow.get("2021-11-16T16:31:00-08:00")) == '2021-11-17T06:01:00-08:00'
    assert close_time(305, 300, arrow.get("2021-11-16T16:31:00-08:00")) == '2021-11-17T12:31:00-08:00'
    assert close_time(405, 400, arrow.get("2021-11-16T16:31:00-08:00")) == '2021-11-17T19:31:00-08:00'
    assert close_time(605, 600, arrow.get("2021-11-16T16:31:00-08:00")) == '2021-11-18T08:31:00-08:00'
    assert close_time(1005, 1000, arrow.get("2021-11-16T16:31:00-08:00")) == '2021-11-19T19:31:00-08:00'

def test_0_600():
    """
    Calculate close time with min speed of 15km/hr between 0-200, 200-400, 400-600 
    """
    assert close_time(75, 200, arrow.get("2021-11-16T16:31:00-08:00")) == '2021-11-16T21:31:00-08:00'
    assert close_time(300, 400, arrow.get("2021-11-16T16:31:00-08:00")) == '2021-11-17T12:31:00-08:00'
    assert close_time(560, 600, arrow.get("2021-11-16T16:31:00-08:00")) == '2021-11-18T05:51:00-08:00'

def test_600_1000():
    """
    Calculate times that use a min speed of 11.428
    """
    assert close_time(700, 1000, arrow.get("2021-11-16T16:31:00-08:00")) == '2021-11-18T17:16:00-08:00'
    assert close_time(730, 1000, arrow.get("2021-11-16T16:31:00-08:00")) == '2021-11-18T19:54:00-08:00'
    assert close_time(800, 1000, arrow.get("2021-11-16T16:31:00-08:00")) == '2021-11-19T02:01:00-08:00'
    assert close_time(950, 1000, arrow.get("2021-11-16T16:31:00-08:00")) == '2021-11-19T15:09:00-08:00'
