"""
Nose tests for letterbag.py
"""

from acp_times import open_time
import arrow
import nose    # Testing framework
import logging
logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.WARNING)
log = logging.getLogger(__name__)


def test_simple_str():
    """
    Calculate open times for each brevet length
    """
    assert open_time(120, 200, arrow.get("2021-11-16T16:31:00-08:00")) == '2021-11-16T20:03:00-08:00'
    assert open_time(150, 300, arrow.get("2021-11-16T16:31:00-08:00")) == '2021-11-16T20:56:00-08:00'
    assert open_time(300, 400, arrow.get("2021-11-16T16:31:00-08:00")) == '2021-11-17T01:31:00-08:00'
    assert open_time(410, 600, arrow.get("2021-11-16T16:31:00-08:00")) == '2021-11-17T04:59:00-08:00'
    assert open_time(890, 1000, arrow.get("2021-11-16T16:31:00-08:00")) == '2021-11-17T21:40:00-08:00'

